package no.noroff;

import org.json.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

public class Connection {
    public final String URL = "https://anapioficeandfire.com/api/characters/";
    String lastCharactersHouse;

    // Precode
    private String requestURL(String url) throws Exception{
        // Set URL
        URLConnection connection = new URL(url).openConnection();
        // Set property - avoid 403 (CORS)
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        // Creat connection
        connection.connect();
        // Create a buffer of the input
        BufferedReader buffer  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        // Convert the response into a single string
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = buffer.readLine()) != null) { // buffer.ready() works here too?
            stringBuilder.append(line);
        }
        return stringBuilder.toString();
    }

    /*
    * Finds character by URL. This has to be character URL.
    * If isStoreHouse then we save the house of the character. This
    * is set to true when called from UserInteraction, and false when
    * called in this class. This makes it so that multiple calls to see
    * sworm members of a single house can be called after one another without
    * having to search for that character first every time.
    */
    public String find(String url, boolean storeHouse){
        JSONObject jo;
        try {
            jo = new JSONObject(requestURL(url));
        }catch (Exception e){
            System.out.println(e.getMessage());
            return e.getMessage();
        }

        if(!jo.isEmpty()){
            JSONArray allegiances = new JSONArray(jo.get("allegiances").toString());
            if(allegiances.length() > 0 && storeHouse)
                this.lastCharactersHouse = allegiances.get(0).toString();
            return jo.get("name").toString();
        }


        return "Not found";
    }

    /*
    * Finds Sworn members of lastCharacterHouse. Since this function is depending on
    * a search for a character to be completed i stored the allegiances URL at index 0
    * to be used here later.
    */
    public ArrayList<String> findSwornMembers(){
        JSONObject jo;
        try {
            jo = new JSONObject(requestURL(this.lastCharactersHouse));
        }catch (Exception e){
            System.out.println(e.getMessage());
            return new ArrayList<String>();
        }
        JSONArray characters = new JSONArray(jo.get("swornMembers").toString());


        ArrayList<String> loyalCharacters = new ArrayList<>();
        loyalCharacters.add(jo.get("name").toString());
        characters.forEach(e -> loyalCharacters.add(find(e.toString(), false)));

        return loyalCharacters;
    }

    /*
    * Returns a hashmap with key == bookname and ArrayList<String> == characters
    * Runs through all books and filters out the books not published by Bantam Books.
    */
    public HashMap<String, ArrayList<String>> povInBook(){
        String url = "https://anapioficeandfire.com/api/books/";
        HashMap<String, ArrayList<String>> bookPovCharacters = new HashMap<>();
        ArrayList<String> bookNames = new ArrayList<>();

        JSONObject jo;
        int counter = 1;
        try {
            while(true){
                jo = new JSONObject(requestURL(url+counter++));
                if(!jo.get("publisher").toString().equals("Bantam Books"))
                    continue;

                String bookName = jo.get("name").toString();
                if(!bookNames.contains(bookName))
                    bookNames.add(bookName);
                ArrayList<String> characters = new ArrayList<>();
                JSONArray characterLinks = new JSONArray(jo.get("povCharacters").toString());

                characterLinks.forEach(e -> characters.add(find(e.toString(), false)));
                if(characters.size() == 0)
                    bookPovCharacters.put(bookName, new ArrayList<>());

                characters.stream().forEach(e-> {
                    if(bookPovCharacters.containsKey(bookName)){
                        bookPovCharacters.get(bookName).add(e);
                    }else {
                        bookPovCharacters.put(bookName, new ArrayList<String>());
                        bookPovCharacters.get(bookName).add(e);
                    }
                });

            }
        }catch (Exception e){
            // this catch is used to end the loop, as i didn't know how else to do it
            bookNames.forEach(n -> {
                for(int i = 0; i < bookPovCharacters.get(n).size(); i++)
                    System.out.println(bookPovCharacters.get(n).get(i));
            });
            return bookPovCharacters;
        }
    }


}
