package no.noroff;

import java.util.ArrayList;

public class CharactersInBook {
    private String bookName;
    private ArrayList<String> characters;

    public CharactersInBook(String bookName){
        this.bookName = bookName;
        characters = new ArrayList<>();
    }

    public String getBookName() {
        return bookName;
    }

    public ArrayList<String> getCharacters() {
        return characters;
    }

    public void appendCharacters(String chara) {
        this.characters.add(chara);
    }


    public boolean hasCharacter(String name){
        return this.characters.contains(name);
    }
}
