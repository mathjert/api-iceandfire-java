package no.noroff;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class UserInteraction {
    //TODO: user interaction
    Connection c;

    public UserInteraction(){
        c = new Connection();
    }

    void getInput(){
        Scanner userInput = new Scanner(System.in);
        OUTERLOOP: do{
            System.out.print(menu());
            try {
                int input = Integer.parseInt(userInput.nextLine());
                switch (input){
                    case 1:
                        System.out.print(findCharacterMenu());
                        int charaInput = Integer.parseInt(userInput.nextLine());
                        System.out.println(c.find(c.URL + charaInput, true));
                        try{
                            INNERLOOP: do{
                                System.out.print(swornMembersMenu());
                                charaInput = Integer.parseInt(userInput.nextLine());
                                switch (charaInput){
                                    case 1:
                                        HashMap<String, ArrayList<String>> tmp = new HashMap<>();
                                        ArrayList<String> tmp2 = c.findSwornMembers();
                                        if(tmp2.size() > 0){
                                            tmp.put(tmp2.remove(0), tmp2);
                                            System.out.println(makeGrid(tmp));
                                            break;
                                        }
                                        System.out.println("No House found");
                                        break;
                                    case 2:
                                        break INNERLOOP;
                                    case 3:
                                        break OUTERLOOP;
                                    default:
                                        System.out.println("Invalid input");
                                        break;
                                }
                            }while (true);
                        }catch (Exception e){
                            System.out.println("Invalid input");
                        }
                        break;
                    case 2:
                        HashMap<String, ArrayList<String>> ret = c.povInBook();
                        System.out.println(makeGrid(ret));
                        break;
                    case 3:
                        break OUTERLOOP;
                    default:
                        System.out.println("Invalid input");
                }
            } catch (Exception e){
                System.out.println("Invalid input\n");
            }

        } while(true);
        userInput.close();
    }
    private String menu(){
        return ("1. Find character by number \n2. See all POV characters from the books published by Bantam Books\n3. exit\n>");
    }
    private String findCharacterMenu(){
        return ("Enter the Character number (i.e 583 is Jon Snow)\n>");
    }
    private String swornMembersMenu(){
        return ("1. See sworn members of this characters house\n2. Go back to main menu\n3. exit\n>");
    }

    private String makeGrid(HashMap<String, ArrayList<String>> input){
        CharactersInBook[] printArray = new CharactersInBook[input.size()];
        int counter = 0;
        StringBuilder finalGrid = new StringBuilder();
        for(Map.Entry<String, ArrayList<String>> e : input.entrySet()){
            printArray[counter] = new CharactersInBook(e.getKey());
            for(String name : e.getValue()){
                printArray[counter].appendCharacters(name);
            }
            counter++;
        }
        int coloumnWidth = 45;
        String $45Saces = "                                             "; // 30 spaces for alignment
        String $93dashes = "---------------------------------------------------------------------------------------------"; // separator
        finalGrid.append($93dashes).append("\n");
        for(CharactersInBook e : printArray){
            finalGrid.append($93dashes).append("\n").append($93dashes).append("\n");
            if(e.getCharacters().size() == 0){
                finalGrid.append("|").append(e.getBookName()).append("\n").append($93dashes).append("\n");
                continue;
            }
            int midWay = e.getCharacters().size()/2, countCharacters = 0;
            for(int i = 0; i < e.getCharacters().size(); i++){
                finalGrid.append("|");
                if(i == midWay){
                    for(int j = 0; j < coloumnWidth; j++){
                        if(j >= e.getBookName().length()){
                            finalGrid.append(" ");
                        }else {
                            finalGrid.append(e.getBookName());
                            j += e.getBookName().length()-1;//not sure, but had to have this for alignment
                        }
                    }
                    finalGrid.append("|");
                }
                    if(i != midWay)
                        finalGrid.append($45Saces).append("|");
                    for(int j = 0; j < coloumnWidth; j++){
                        if(j >= e.getCharacters().get(countCharacters).length()){
                            finalGrid.append(" ");
                        }else {
                            finalGrid.append(e.getCharacters().get(countCharacters));
                            j += e.getCharacters().get(countCharacters).length();
                        }
                    }
                    countCharacters++;
                finalGrid.append("|\n");
                finalGrid.append($93dashes).append("\n");
            }
        }
        return finalGrid.toString();
    }
}
